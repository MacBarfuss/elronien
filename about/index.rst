########################################
     Über
########################################

.. toctree::
    :hidden:

    content
    licence
    legal
    contribution

Über das Werk, die Macher und das Copyright.
