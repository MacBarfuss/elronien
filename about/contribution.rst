########################################
     Mitwirkung
########################################

Gerne kann an der Gestaltung von Elronien mitgewirkt werden. Was dazu erforderlich ist, habe ich hier zusammengefasst.

=================================================
     Generelle Vorgehensweise und Prinzipien
=================================================

Alle Texte stehen unter einer CC-Lizenz, die auch als Free-Culture-Lizenz gültig ist. Die Texte dürfen also frei verwendet werden.
Anstatt aber eine eigene Welt davon abzuleiten, möchte ich dazu ermutigen Veränderungen in diese Welt einzubringen.

Dazu kann per git ein Merge-Request erstellt werden und ich übernehme die Änderungen dann in mein Repository.
Die jeweiligen Autoren werden dann selbstverständlich erwähnt.

========================================
     Vorgehensweise im Detail
========================================

Am einfachsten ist es mir eine Nachricht zu schreiben. Dazu bitte das Kontaktformular benutzen.
Ich erstelle dann einen Account auf dem Server, auf dem die Informationen abgelegt sind.
Dann kann ein Projekt von meinem Projekt abgezweigt werden.

Die Veränderungen könnnen dann dort frei gepflegt werden und dann als Pull-Request an mich gesendet.
