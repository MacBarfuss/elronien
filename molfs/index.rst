########################################
     Die Molfen
########################################

.. toctree::
    :hidden:

    magic
    government

..    customs
    languages
    measurement
    education
    science
    celebrities

.. structure:
    'elrons' => array('Molfe', 'Die Molfen', array(
        'customs' => array('Bräuche', ''),
        'food' => array('Ernährung', ''),
        'languages' => array('Sprachen', ''),
        'education' => array('Bildung', ''),
        'dresses' => array('Kleidung', ''),
        'science' => array('Wissenschaft', ''),
        'magic' => array('Magie', ''),
        'calendar' => array('Kalender', ''),
        'bodilylanguage' => array('Körpersprache', ''),
        'ethics' => array('Ethik', ''),
        'religion' => array('Religion', ''),
        'government' => array('Regierung', ''),
        'politics' => array('Politik', ''),
        'genderroles' => array('Geschlechterrollen', ''),
        'music-and-art' => array('Musik und Kunst', ''),
        'architecture' => array('Architektur', ''),
        'military' => array('Militär', ''),
        'technology' => array('Technologie', ''),
        'courtship' => array('Rechtsprechung', ''),
        'demography' => array('Demografie', ''),
        'transportation' => array('Transportwesen', ''),
        'medicine' => array('Medizin', ''),
        'history' => array('Geschichte', ''),
        'celebrities' => array('Berühmtheiten', ''),
    )),

Die Molfen sind überwiegend magiebegabte Personen und allesamt Nachfahren von Elronieren, welche verstoßen wurden
oder sich freiwillg vom Elronischen Volk abgewendet haben. Aus sich der Elronier sind sie Abtrünnige.

Die Gesellschaft ist sehr lose und den Molfen ist die persönliche Freiheit sehr wichtig.

Es gibt unterschiedliche Formen des Zusammelebens:
  * Großfamilien (teilweise auch mit Inzucht)
  * Kommunen
  * Klausner
  * Nomaden

Die Molfen selbst haben keine Industrie. Sie leben sehr stark mit der Natur verbunden und nutzen die Möglichkeiten,
die der Dunkelwald ihnen bietet.

Molfen gelten als die unanfechtbaren Herrscher innerhalb des Dunkelwaldes. Der Dunkelwald kann im Prinzip nicht ohne
die Zustimmung der Molfen betreten oder gar passiert werden.

Da die Wurzeln bei den Elroniern liegen, kann bei hier nicht aufgeführten Themen die jeweilige Beschreibung der
Elronier zu Rate gezogen werden.

