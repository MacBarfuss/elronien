########################################
     Regierung
########################################

========================================
     Souveränität
========================================

Beteiligung an der Führung steht jedem Molfen offen. Dazu muss man lediglich mit Personen in Kontakt treten, die sich
bereits in die Führung einbringen. Alle Personen die aktiv sind, werden gemeinsam als der Zirkel bezeichnet. Die
Aufnahme unterliegt keiner Formalie. Die Anzahl der Mitglieder im Zirkel ist nicht begrenzt.

Der Zirkel wird von „der Hand“ geführt. „Die Hand“ sind |Hand| gewählte Personen. Gewählt wird auf Lebenszeit und
jeder Molfe egal welchen Alters darf wählen. Üblicherweise werden ältere Sesshafte Molfe gewählt, die sich bereits
stark im Zirkel engagiert hatten. Die Abwahl ist möglich, aber sehr unüblich.

|Hand| wurde als Zahl gewählt, damit wenige Patt-Situationen bei Abstimmungen vorkommen können. Außerdem zeigt es das
Verständniss, dass „die Hand“ nicht als eiserne Faust regieren kann und eben auch nicht vollkommen ist.

Abstimmungen werden wann immer möglich nach dem Konsent-Prinzip getroffen. Einwände werden gesammelt und versucht in
die Lösung einzubringen, bis bei der Abstimmung keine Gegenrede mehr gestellt wird.

========================================
     Verwaltung
========================================

Verwaltung im modernen Verständnis der Elronier gibt es nicht.
