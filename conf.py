# Configuration file for the Sphinx documentation builder.
# More options: https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -------------------------------------------------------

project = 'Elronien'
copyright = '2020, MacBarfuss'
author = 'MacBarfuss'

# The full version, including alpha/beta/rc tags
version = 'v0.1'
release = 'v0.1-alpha'


# -- General configuration -----------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
]

templates_path = ['_templates']

language = 'de'

exclude_patterns = ['_build', 'venv']

suppress_warnings = ['epub.unknown_project_files']


# -- Options for EPUB output ---------------------------------------------------

# epub_tocdepth = 3

# epub_tocdup = False # default is True

epub_tocscope = 'includehidden'


# -- Options for HTML output ---------------------------------------------------

html_theme = 'classic'

# html_theme_options = {
#     "rightsidebar": "false",
# "relbarbgcolor": "black"
# }

html_static_path = ['_static']
html_css_files = ['sixcount.css']

html_sidebars = {
    '**': ['globaltoc.html', 'localtoc.html', 'searchbox.html', 'licence.html'],
}

html_secnumber_suffix = " "

# -- Global substitutions ------------------------------------------------------
rst_prolog = """

.. |^2| replace:: :sup:`2`

.. |^3| replace:: :sup:`3`

.. |0| raw:: html epub

    <span class="number">0</span>

.. |1| raw:: html epub

    <span class="number">1</span>

.. |2| raw:: html epub

    <span class="number">2</span>

.. |3| raw:: html epub

    <span class="number">3</span>

.. |4| raw:: html epub

    <span class="number">4</span>

.. |5| raw:: html epub

    <span class="number">5</span>

.. |6| raw:: html epub

    <span class="number">6</span>

.. |Ding| raw:: html epub

    <span class="number">1</span>

.. |Hand| raw:: html epub

    <span class="number">5</span>

.. |Faust| raw:: html epub

    <span class="number">6</span>

.. |Kerl| raw:: html epub

    <span class="number">66</span>

.. |Haufen| raw:: html epub

    <span class="number">100</span>
"""
