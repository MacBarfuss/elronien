###################################################
     Beschreibung der Landschaft und der Natur
###################################################

.. toctree::
    :hidden:

    geology
    topography
    astronomy
    climate
    perils

.. structure:
    'physical' => array('Land und Natur', 'Beschreibung der Landschaft und der Natur', array(
        'geology' => array('Geologie', 'Entwicklungsgeschichte der Region'),
        'topography' => array('Topografie', 'Geländekunde'),
        'astronomy' => array('Astronomie', 'Gestirne'),
        'climate' => array('Klima', 'Klima und typisches Wetter'),
        'ressources' => array('Ressourcen', 'Gesteine, Metalle, ...'),
        'flora' => array('Pflanzenwelt', 'Flora'),
        'wildlife' => array('Tierwelt', 'Fauna'),
        'races' => array('Bewohner', 'Spezifische Merkmale der Einheimischen'),
        'diseases' => array('Krankheiten', 'bestimmte für die Region spezielle Krankheiten und Gefahren'),
        'magical places' => array('Magische Orte', ''),
        'magical wildlife' => array('Magische Tiere', ''),
        'magical objects' => array('Artefakte', ''),
    )),