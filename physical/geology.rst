########################################
     Geologie
########################################

.. Aufbau, Zusammensetzung und Struktur der Oberfläche und des Bodens, der Eigenschaften der Gesteine und der Entwicklungsgeschichte sowie der Prozesse, welche zu diesen Eigenschaften geführt haben und heute noch Veränderungen bewirken.

Sehr groß gedacht handelt es sich bei der Region um eine kleine Delle in der Weltenmembran. Diese wird im wesentlichen von einem großen See ausgefüllt. Der See wird von sehr vielen Bächen und Flüssen aus den sehr unterschiedlichen Landschaften gespeist. Ihn verlässt ein großer Strom in [nordwestlicher] Richtung auf stark geschlängeltem Weg. Außerdem fließt ein Teil des Wassers in große Sümpfe. [Oder fließt alles in die Sümpfe und Elron ist der größte Zufluss?]

========================================
     Entstehungsgeschichte
========================================

Es ist nicht geklärt, jedoch gibt es Mythen darüber, dass eine göttliche Kraft einst an der Stelle des heuten Elronsee gewirkt haben muss. Dabei wurde nicht nur das Gelände an einer Stelle herabgedrückt. Die Spannungen im Erdreich und seiner Gesteine haben sich auf die umliegenden Flächen fortgesetzt. Wo hartes Gestein mit reichhaltigen Erzvorkommen vorgeherrscht haben, wurde das Erdreich nach oben gedrückt. Dabei sind teilweise sehr bizarre Strukturen entstanden. Wo große Mengen Wasser im Boden eingeschlossen war, sind diese Vorkommen aufgebrochen und haben sich mit dem Erdreich vermischt. Sümpfe sind entstanden. An einer Stelle ist die Membran regelrecht gebrochen und hat den Weg für den Fluß Elron vorbestimmt. Durch die Neigung eines Teils stärker zum Tagesgestirn hin, hat sich ein ehemals reich bewachsenes Areal zunehmend zu Steppe und Wüste gewandelt.

========================================
     Aktuelle Veränderungsprozesse
========================================

Derzeit gibt es keine bekannten Veränderungen und keine bekannten Instabilitäten.
