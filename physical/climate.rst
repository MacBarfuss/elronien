########################################
     Klima
########################################

Die Monate trennen auch die Jahreszeiten voneinander ab. Der Begriff „Jahreszeit“ ist nicht bekannt.

.. csv-table::

    Frostmonat, Winter
    Blütemonat, Frühling
    Wonnemonat, Frühsommer
    Sonnenmonat, Sommer
    Erntemonat, Frühherbst
    Nebelmonat, Spätherbst
