########################################
     Astronomie
########################################

========================================
     Der Lebenstakt
========================================

Tag und Nacht
####################

Tag und Nacht haben eine durchschnittliche Länge und sind etwa gleich lang. Am Tag scheint eine gelbliche Sonne. In der Nacht stehen zwei Monde am Himmel, ein blauer und ein roter Mond. Diese sind etwa gleich groß.

Jahreszeiten
####################

Zwar bleibt der Lauf der Sonne immer auf gleicher Bahn über dem Boden, jedoch ändert sich ihre Intensität im Lauf
eines Zyklus. Dieser hat 216 Tage. Das sind 6 * 6 * 6 Tage. 6 Tage nennt man Woche, 36 Tage nennt man Monat.

Die Monate nennt man Frostmonat, Blütemonat, Wonnemonat, Sonnenmonat, Erntemonat und Nebelmonat. Der Frostmonat ist
sehr kalt und der Sonnenmonat sehr heiß.

Die Gestirne
####################

Die Sonne
====================

Der rote Mond
====================

Der rote Mond folgt exakt der Sonnenlaufbahn, nur um die halbe Länge von Tag und Nacht verschoben. Wo die Sonne zur Mittagsstunde steht, dort steht der Mond um Mitternacht.

Der blaue Mond
====================

Sterne
====================

Es gibt mehrere ortsfeste Sternbilder. Diese können gut zur Orientierung in der Nacht genutzt werden.

Manche Sternbilder ändern sich im Jahreszyklus.
