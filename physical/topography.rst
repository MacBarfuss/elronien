########################################
     Topografie
########################################

=========================================
     Grobgliederung und Orientierung
=========================================

Als gröbste Orientierungsmerkmale dienen die Lage des Elronsee und des Flusses Elron. Der Elronsee bildet das
zentrale Herzstück dieser Provinz. Vom Elron ausgehend mit dem See zur rechten Hand passiert man die Hälfte der großen
Ebene und kommt in die Hügellande. Danach kommen die großen Berge, die bis an den See heran reichen. Dahinter kommt der sehr dichte und nur schwer passierbare Dunkelwald. Nach dem Wald kommen die großen Sümpfe. Hier hat man etwa die Hälfte des Weges zurück gelegt. Die Sümpfe schließen direkt an die große Wüste an, welche nach einiger Strecke langsam zur Steppe wird und in die zweite Hälfte der großen Ebene übergeht.

==========================================
     Die große Ebene rechts des Elron
==========================================

Durch denn Fluß Elron und seine Seitenarme ist hier reichlich Wasser vorhanden. Die Böden sind gut für die
unterschiedlichsten landwirtschaftlichen Erzeugnisse. Sie sind nahe des Flußes schwer und werden auf die großen Berge zugehend mineralischer.

========================================
     Die Hügellande
========================================

[viel Schafzucht mit der vollständigen Weiterverarbeitung: Käse, Fleisch, Wolle, Spinnerei, Tuchherstellung, etwas Färberei; die Produktion von Getreide fließt in die benachbarten Berge und Wälder]

Die Durchquerung der Hügellande dauert zu Pferd etwa Stand Tage und ist auch mit Karren oder anderen großen
Fahrzeugen gut möglich.


Die Höhlen von Rach'noz
########################################

Hier verbergen sich unter einer Hügelkette eine Gruppen von großen Höhlen. Diese waren einst von Drachen bewohnt.

========================================
     Der große See und seine Ufer
========================================

Der See ist nicht sehr tief, birgt für Schifffahrt nur wenig Gefahren wie z.B. Untiefen.

Die Ufer sind stark unterschiedlich und hängen von den jeweiligen angrenzenden Landschaften ab. Flache Sandstrände
hinter Dünen bei den großen Ebenen, Klippen in den Hügellanden, bei den Bergen steht der Fels direkt im Wasser,
steilere kurze Sandstrände beim Dunkelwald, Morastige Ausläufer beim Moor, die Elronwüste geht direkt ins Meer über.

========================================
     Die großen Berge
========================================

Das Passieren der großen Berge ist nahezu unmöglich. Die Gipfel sind sehr hoch und oft sind die Felswände sehr
steil. Bewohner gibt es wenige. Neben sehr kargen Möglichkeiten für Flora und Faune haben sich wenige Klausner in
die Berge zurückgezogen.

========================================
     Der Dunkelwald
========================================

sehr dichter und unwegsamer Dschungel. Die Molfen leben hier.

========================================
     Die großen Sümpfe
========================================

========================================
     Die Wüste
========================================

========================================
     Die elronische Steppe
========================================

=========================================
     Die große Ebene links des Elron
=========================================

Wie ihre Schwesterebene rechts des Elron sind auch hier die Böden am Fluß schwer und bieten viele Möglichkeiten der Landwirtschaft. Je weiter man sich jedoch vom Fluß entfernt und auf die Steppe zugeht, desto sandiger werden hier jedoch die Böden.
