.. Elronien documentation master file, created by
   sphinx-quickstart on Sun Aug 30 11:39:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Elronien's documentation!
====================================

.. toctree::
    :maxdepth: 2
    :glob:
    :numbered:

    intro
    about/index
    context/index
    Land und Natur <physical/index>
    Elronier <elrons/index>
    Molfe <molfs/index>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. This is my structure for headlines in my documents:
   ### over and under
         and with indentation of 4 characters
         and at least 40 characters long and 4 characters after last letter of caption
   === over and under
         and with indentation of 4 characters
         and at least 40 characters long and 4 characters after last letter of caption
   ### at least 20 characters long and 4 characters after last letter of caption
   === at least 20 characters long and 4 characters after last letter of caption
   """ with same size as caption
   --- with same size as caption
   ... with same size as caption
