########################################
     Sprachen
########################################

========================================
     Elronisch
========================================

Anreden
####################

Du unter Bekannten, Sie gegenüber Fremden, Euch/Ihr gegenüber Edelleuten.


Besondere Formuluierungen durch das eigene Zahlsystem
##########################################################

.. csv-table::
    :header: Begriff, Ersatz, Anmerkungen
    :widths: 1,2,4

    allein,
    ein, ding, kann oft auch weggelassen werden
    einfach, dinglich
    Einigkeit, "Konsens, Gleichstimmigkeit"
    einsam, abgeschieden
    Einsamkeit, Abgeschiedenheit
    Einsiedelei, Klause
    Einsiedler, Klausner
    einzeln, "dingen, dinglich, getrennt"
