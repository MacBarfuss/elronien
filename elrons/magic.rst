########################################
     Magie
########################################

Magie oder magische Begabung ist selten in Elronien.


========================================
     Regulierung
========================================

Das magische Geschehen in Elronien unterliegt keiner Regulierung.


=======================================================
     Organisationen und gesellschaftliche Stellung
=======================================================

Nur wenige magisch begabte Personen haben sich zu Zirkeln zusammengeschlossen. Es gibt keinen Zirkel, der für das
öffentliche Leben allgemein relevant ist.


========================================
     Fähigkeiten und Typen
========================================

Da magische Begabung sehr selten ist, stehen viele Elronier Magie allgemein mit Skepsis und Ängsten gegenüber.

Manche Magiebegabte werden daher auch verstoßen und sterben schon als Kinder oder leben Abseits in einer Klause.

Wenigen Priestern sagt man außergewöhnliche Gunst der Götter nach und genießen daher sehr hohes Ansehen. Sie sind
starke Karma-Magier.

Psi-Magie, z.B. in Form von Hellsicht, wird gelegentlich beobachtet.

Äther-Magie ist fast vollständig unbekannt, auch wenn diese bisweilen vorkommt.
