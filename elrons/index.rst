########################################
     Die Elronier
########################################

.. toctree::
    :hidden:

    customs
    languages
    measurement
    education
    science
    magic
    government
    celebrities
    transportation

.. structure
    'elrons' => array('Elronier', 'Die Elronier', array(
        'customs' => array('Bräuche', ''),
        'food' => array('Ernährung', ''),
        'languages' => array('Sprachen', ''),
        'measurment' => Maßsystem
        'education' => array('Bildung', ''),
        'dresses' => array('Kleidung', ''),
        'science' => array('Wissenschaft', ''),
        'magic' => array('Magie', ''),
        'calendar' => array('Kalender', ''),
        'bodilylanguage' => array('Körpersprache', ''),
        'ethics' => array('Ethik', ''),
        'religion' => array('Religion', ''),
        'government' => array('Regierung', ''),
        'politics' => array('Politik', ''),
        'genderroles' => array('Geschlechterrollen', ''),
        'music-and-art' => array('Musik und Kunst', ''),
        'architecture' => array('Architektur', ''),
        'military' => array('Militär', ''),
        'technology' => array('Technologie', ''),
        'courtship' => array('Rechtsprechung', ''),
        'demography' => array('Demografie', ''),
        'transportation' => array('Transportwesen', ''),
        'medicine' => array('Medizin', ''),
        'history' => array('Geschichte', ''),
        'celebrities' => array('Berühmtheiten', ''),
    )),