########################################
     Bildung und Ausbildung
########################################

========================================
     Schule
========================================

Allgemeine Schulpflicht gibt es nicht. Generell gibt es kein breites ausgebautes Schulsystem. Für die grundlegende
Ausbildung in Lesen und Schreiben, sowie Mathematik und Sachkunde ist das eigene Elternhaus verantwortlich.
Dieser Verantwortung kann auch nachgekommen werden, in dem das Kind auf eine Privatschule gegeben wird. Die Kosten
sind unterschiedlich je Schule, aber in der Regel sehr hoch.

.. index::
    pair: Zünfte; Ausbildung

========================================
     Ausbildung
========================================

Die wirkliche Bildung liegt in den Händen der Zünfte. Diese unterhalten Schulen zur Ausbildung ihrer Lehrlinge.

Es gibt unterschiedliche Voraussetzungen zur Aufnahme in eine Zunft-Schule. Manche Schulen setzen zum Beispiel
voraus, dass die zukünftigen Lehrlinge bereits gut Lesen und Schreiben können.

Auf der Zunftschule wird immer zuerst der Titel des Gesellen erreicht.
Danach kann nach ausreichender praktischer Erfahrung die Weiterbildung zum Meister angetreten werden.

.. TODO Zünfte irgendwo erklären


========================================
     Akademische Bildung
========================================

Viele Zünfte unterhalten neben ihren Schulen auch eine Akademie. Dort werden die Großmeister ausgebildet und die
Weiterentwicklung des Handwerks betrieben.

Die Administration unterhält die „Elronische Akademie“, welche in mehreren Institutionen die staatlich geförderten
Forschungen durchführt.
