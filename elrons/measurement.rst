########################################
     Maße und Einheiten
########################################

========================================
     Größe
========================================

Längen
####################

Fingerdick, Fingerlang, Elle, Schritt und Meile sind die Maße für Längen.

Finger, Elle und Schritt orientieren sich am Körperbau der Elronier.

|Ding| Meile sind |1|\ |0|\ |0|\ |0|\ |0| Schritte.

Flächen
####################

Alle Flächen tragen „Blatt-“ als Vorsilbe, wenn sie rechtwinklig ausgemessen sind.

Zusätzlich gibt es den Morgen, der nur in der Landwirtschaft genutzt wird. |Ding| Morgen ist das |1|\ |0|\ |0|\
|0|-Schritte-Blatt, also das Blatt mit der Seitenlänge von |1|\ |0|\ |0|\ |0| Schritte.

Volumen
####################

Becher, Humpen, Flasche, Krug und Faß sind die Maße für Volumen.

Fässer werden als Faust-Ecke angelegt.

========================================
     Zeit
========================================

Stunde, Minute, Atemzug und Herzschlag sind die Maße für Zeit.
