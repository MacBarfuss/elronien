########################################
     Regierung
########################################

========================================
     Souveränität
========================================
[Regierungsform ist parlamentarische Republik; Administration in der Hauptstadt mit mehrere Organen; es wird versucht einiges zu verstaatlichen, was nur zum Teil klappt und zu Konflikten führt; die Leitung staatlicher Einrichtung liegt immer bei Edelleuten, wenn diese ihren Vorteil über das Allgemeinwohl stellen entsteht Ausbeutung; Außerdem gibt es Konflikte zu Organen mit „unedlen“ Leitungen, zu unrecht oder zurecht nicht geedelt]

========================================
     Verwaltung
========================================

Seit Ausrufung der Republik sucht man verzweifelt nach dem richtigen Weg den Staatsangelegenheiten Herr zu werden.
Mancherlei Vorgang wird durch Bürokratie fast unmöglich gemacht. In der Zeit der Fürsten und Herzöge unterlag die Verwaltung dem jeweiligen Lokalherrscher. Die Zentralisierung stellt die Verwaltung vor große Herausforderungen. Derzeit erwägt man die Verwaltung wieder in mehrere lokale Verwaltungen zu unterteilen.

Aktuell gibt es das zentrale Standesamtswesen. Das soll zukünftig wieder in die Hand der Ortsvorsteher. Jede Geburt und jeder Todesfall muss aktuell an die Administration gemeldet werden.
