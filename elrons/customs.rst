########################################
     Bräuche
########################################

========================================
     Namen
========================================

Bei Geburt wird einem Kind ein Rufname vergeben; als Beiname wird später der Beruf oder ein sonstiger gewählter Beiname genutzt. Dabei kann man schon in Verruf geraten, wenn man sich selbst einen großen Namen gibt, den Andere nicht anerkennen. Kinder werden als „der kleine …“ oder „… von … (voller Name der Eltern)“ bezeichnet. Befindet man sich an einem anderen Ort als der Heimat, kann auch die Herkunft als Bezeichnung genutzt werden, entweder als „… aus Duseldorf“ oder „… Buchenhainer“.

========================================
     Edelleute
========================================

Die edlen Leute erhalten ihren Stand durch Ernennung. Die Ernennung erfolgt ausschließlich auf der Basis des eigenen Tuns. Die Person muss unter Beweis stellen, dass ihr das Allgemeinwohl am Herzen liegt und sich dafür aktiv einsetzen. Wer zum Edelmann oder zur Edelfrau ernannt ist, darf (und muss auch) diesen Titel dem Namen voran stellen. Gehen wir als Beispiel vom Ehepaar Elisa und Mattis Georgianer aus. Diese heißen nach der Ernennung zu Edelleuten "Edelfrau Elisa Georgianer", „Edelmann Mattis Georgianer“ und gemeinsam „Edelleute Elisa und Mattis Georgianer“. Wird nur eine Person geedelt, wird der Partner bei gemeinsamer Nennung nachgestellt: „Edelfrau Elisa Georgianer und ihr Mann Mattis Georgianer“.

Der Titel ist nicht vererbbar, jedoch können Nachfahren ihren Namen mit Zusatz ergänzen. Dieser wird nach dem Vornamen und vor dem Familiennamen eingeschoben. Aus „Magnus Georgianer“ wird „Magnus, Sohn der edlen Georgianer“. Die Angabe des Zusatzes ist freiwillig und darf situativ erfolgen. Ist es nicht ausreichend genau welche edle Person die Ahnin ist, kann der Vorname mit aufgenommen werden, also „Magnus, Sohn der edlen Elisa Georgianer“. Enkel von Edelleuten dürfen ihrem vollen Namen „Kind aus edlem Haus“ oder (im obigen Fall) „Kind aus dem edlen Haus Georgianer“ hinten anstellen. Spätere Nachkommen dürfen sich nicht mehr auf den Titel berufen. Wird das Kind von Edelleuten selbst wieder zur edlen Person ernannt (und die Eltern werden die Erziehung ihres Kindes entsprechend ausrichten), so kann es zu vollständigen Namen kommen wie „Edelfrau Katharina Schmied, Tochter des edlen Magnus Georgianer“.

Edelleute haben keine offizielle politische oder gesellschaftliche Funktion. Jedoch ist ihr Ansehen hoch und sie
stellen Vertrauenspersonen dar. Die heutige Situation hat sich entwickelt, da in der Vergangenheit Stände zur
Ausübung von Macht, angefeuert durch einen Despoten [!!! Link zur Historie] missbraucht wurden. Auch heute gibt es
bestimmte Kreise von Edelleuten, die versuchen ihre Prominenz zum eigenen Vorteil noch vor dem Allgemeinwohl zu
nutzen. Die Edelung muss der Administration mit Begründung vorgeschlagen und von dieser genehmigt werden. Man strebt
einen Anteil an der Bevölkerung von |Faust| bis |Kerl| pro |Haufen| (6 bis 12 pro 216, also 2,78% bis 5,56%) an, wobei
diese Grenzen kein Gesetz sind und immer wieder nach oben oder unten überschritten werden. Die Edelung erfolgt durch Edelleute. Ist dies nicht möglich übernimmt der Kanzler. Das Entedeln von Edelleuten ist möglich, aber selten. Es bedeutet die totale Ächtung der Personen und wird daher nur bei sehr grobem Fehlverhalten vollzogen. Theoretisch ist es denkbar, dass Entedelung auch zu Unrecht geschieht. Die Rehabilitierung ist derzeit nicht vorgesehen.
