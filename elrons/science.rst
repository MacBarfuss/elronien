########################################
     Wissenschaft
########################################

========================================
     Mathematik
========================================

Die Zahlen
####################

Das gängige Zahlsystem funktioniert auf der Basis von 6. Zur Erläuterung wird hier oft das für den Leser des Dokumentes gängige Dezimalsystem mit den Ziffern 0 bis 9 verwendet. Im Laufe der Einführung wird jedoch zunehmend das hier beschriebene Zahlsystem direkt verwendet. Es gibt folgende Ziffern:


.. csv-table::
    :header: "Ziffer und Varianten", "Entspricht dezimal", sprich
    :widths: 2,1,1

    |0|, 0, Leer
    |1|, 1, Ding
    "|2|, jeder Winkel", 2, Paar
    "|3|, jedes offene Quadrat", 3, Stand
    |4|, 4, Blatt
    "|5|, Diagonale in beliebige Richtung", 5, Hand
    |6|, 6, Faust

Die Basiszahl |6|, also Faust, wird als besondere Zahl wahrgenommen. Sie gilt als Essenz-Zahl. Mathematisch drückt sich
das so aus: Sie ist das Produkt aus |1|, |2| und |3|. Die Zahlen haben dabei folgende Bedeutung: |1| (Ding)
symbolisiert das Gegenständliche an sich, |2| (Paar) steht für die einfachste Verbindung von Dingen und |3| (Stand)
ist die Anzahl Füße, die es benötigt, um einen Gegenstand wie einen Tisch oder Stuhl stabil zu platzieren. Die Basis
allen Schaffens ist demnach das Gegenständliche so zusammenzubringen, dass es eine stabile Form annimmt.
Ordnungszahlen wie „Erstes“ oder „Zweites“ sind nicht bekannt. Es gibt die Bezeichnung „Paar Fäuste“ oder „Kerl“ für
12, bzw. |6| |6| (denn echter Kerl hat Paar Fäuste). „Stand Kerle“ oder „Faust Fäuste“ sind auch „Team“ (36, bzw.
|1|\ |0|\ |0|). „Faust Teams“ sind „Haufen“ (216. bzw. |1|\ |0|\ |0|\ |0|).

Da das Wort „eins“ nicht mehr mit Menge verbunden ist (und auch nicht existiert), muss dies bei der Sprache
berücksichtigt werden. Die Sprache wird dadurch entweder etwas kerniger (wenn man weiterhin vom Ding spricht) oder
etwas eleganter, wenn die Mehrzahl verwendet wird.

Da |6| das eigene Zeichen für die Basis-Zahl ist, also paar Notationen für den Wert 6 gültig sind (|1|\ |0| und
|6|), ergeben sich folgende Vorgehensweisen im Alltag: Strichlisten können in gültigen Zahlzeichen geführt und danach
mit Trick zur richtigen Zahl zusammengesetzt werden, die schriftliche Addition erfolgt durch Zusammenfassen der Zeichen und danach die Überführung der Fäuste in die höhere Ziffer.

Umrechnungshilfe
#####################

.. csv-table::
    :header: "elronisch", "dezimal"
    :widths: 1,1

    |6|, 6
    |Kerl|, 12
    |1|\ |0|\ |0|\ |0|, 216
    |1|\ |0|\ |0|\ |0|\ |0|, 1296

Strichlisten
####################

Eine Abgrenzung von Strichlisten zu Zahlen aus mehreren Zeichen ist nicht nötig, da in Zahlen das Zeichen für Faust
nicht vorkommt. Eine Zählung wird mit |Ding| begonnen, dann wird wiefolgt weiter gezählt, wobei der Pfeil → Zählschritte
darstellt: |1| → |2| → |3| → |4| → |5| → |6|. Dann wird beim nächsten Zählschritt das nächste Zeichen hinzugefügt
und gefüllt: |6| → |6|\ |1| → |6|\ |2| → |6|\ |3| → |6|\ |4| → |6|\ |5| → |6|\ |6|. Jetzt kommt die nächste Ziffer dazu
und so weiter.

Um die Zahl jetzt zusammenzufassen wird in der darunterliegenden Zeile die Anzahl der Fäuste gezählt und die nicht
volle Ziffer wird hinten angestellt. Beispiel: Aus |6|\ |6|\ |3| wird |2|\ |3|. Um den dazugehörigen dezimalen Wert
zu berechnen, rechnen wir 2 * 6 + 3 = 15. Ist die Strichliste sehr lang, muss unter Umständen mit Zwischenschritten
gearbeitet werden:
Aus |6|\ |6|\ |6|\ |6|\ |6|\ |6| |6|\ |6|\ |6|\ |6|\ |6|\ |6| |6|\ |6|\ |6|\ |6|\ |3| wird |6|\ |6|\ |4|\ |3|.
|6| darf in der Zahl nicht stehen bleiben und muss also erneut zusammengefasst werden. Aus |6|\ |6|\ |4|\ |3| wird dann
|2|\ |4|\ |3|. Wir berechnen für das Dezimalsystem:

| 2 * 6\ |^2| + 4 * 6 + 3 = n
| 2 * 36 + 4 * 6 + 3 = n
| 72 + 24 + 3 = n
| n = 99


Einfache Schriftliche Addition
###################################

Die Zahlen werden mit den Ziffern untereinander geschrieben. Dann lassen sich die Ziffern wie beim Zählen mit Strichliste aufsummieren, allerdings nach unten:

.. raw:: html epub

    <span class="number">415</span><br />
    <span class="number">334</span><br />
    <span class="number">215</span><br />
    ————<br />
    <span class="number">656</span><br />
    <span class="number">306</span><br />
    <span class="number">002</span><br />

| im Dezimalsystem entspricht das:
| 4 * 6\ |^2| + 1 * 6 + 5 = 155
| 3 * 6\ |^2| + 3 * 6 + 4 = 130
| 2 * 6\ |^2| + 1 * 6 + 5 = 83
| ————————————
| 6 * 6\ |^2| + 5 * 6 + 6 = 252
| 3 * 6\ |^2| + 0 * 6 + 6 = 114
| 0 * 6\ |^2| + 0 * 6 + 2 = 2

Danach müssen alle Fäuste aufgelöst werden. Dafür wird nur die unterste Ziffer (die keine Faust sein darf, im Zweifelsfall Leer) in die neue Zeile übernommen. Für jede Faust wird in der höheren Ziffer Strich hinzugefügt. Falls neue Fäuste entstehen, müssen diese danach ebenfalls aufgelöst werden.

.. raw:: html epub

    <span class="number white">0</span><span class="number">656</span><br />
    <span class="number white">0</span><span class="number">3</span><span class="number">0</span><span class="number">6</span><br />
    <span class="number white">000</span><span class="number">2</span><br />
    —————<br />
    <span class="number">1362</span><br />
    <span class="number white">00</span><span class="number">1</span><span class="number">0</span><br />
    —————<br />
    <span class="number">1412</span>
    </div>

| 6 * 6\ |^2| + 5 * 6 + 6 = 252
| 3 * 6\ |^2| + 0 * 6 + 6 = 114
| 2 = 2
| ————————————
| 1 * 6\ |^3| + 3 * 6\ |^2| + 6 * 6 + 2 = 362
| 1 * 6 + 0 = 6
| ————————————
| 1 * 6\ |^3| + 4 * 6\ |^2| + 1 * 6 + 2 = 368

Zur Überprüfung kann in jedem Block die Summe der Dezimalzahlen aller Zeilen gebildet werden. Das Ergebnis ist in diesem Fall immer 368, beziehungsweise Ding-Blatt-Ding-Paar.

========================================
     Architektur
========================================

Die in der Mathematik verwendete Basis lässt sich sehr gut in der Architektur aufgreifen. Die Grundfläche vieler
Gebäude wird mit faust Ecken angelegt. Darüber steht das Gewölbe, welches entweder spitz, rund oder selbst als halbes
Fausteck angelegt wird. Am unkompliziertesten ist es das Dach spitz aufzubauen und zur Vollendung des Faustecks Zwischenboden einzufügen. Das ist auch für reine Holzhäuser praktikabel. Für große und wichtige Gebäude mit representativem Charakter haben sich zentrale Säulenanlagen bewährt. Türdurchgänge und verzierte Fenster werden im Idealfall ebenso als halbes Fausteck oben abgeschlossen.

Oft werden aus Kostengründen blatteckige Durchgänge und Öffnungen für Fenster und Türen vorgesehen. Man spricht dann auch vom Fenster- oder Türblatt.

========================================
     Forschung
========================================

In der Hauptstadt gibt es im Umfeld der Administration als allgemein finanziertes Organ auch das <q>Institut zur Entwicklung neuer Verfahren und Werkzeuge</q>. Die Forschung ist in mehrere Entwicklungsfelder gegliedert [evtl. 6?].
